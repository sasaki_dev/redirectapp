Redirect App
=============

Options to set in environment variables
---------------------------------------

* PORT: default 80
* REDIRECT: the URL for the redirect (with our without "http://"). If this is set, other options are ignored.
* PAGE_TITLE: default: 'Server down'
* MESSAGE_HTML: use a custom message (html string) in plain HTML file.

```
{
  "custom_env": {
      "REDIRECT": "www.google.com"
  }
}
```

```
{
  "custom_env": {
      "PAGE_TITLE": "Be back soon",
      "MESSAGE_HTML": "<h1>Be back soon</h1><p>Sorry for the inconvenience</p>"
  }
}
```