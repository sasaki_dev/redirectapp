var http = require("http");

var _port = process.env.PORT || 80;

http.createServer(function (req, res) {
    var redirectUrl = process.env.REDIRECT;

    if (redirectUrl) {
        if (redirectUrl.indexOf('://') < 0) {
            redirectUrl = 'http://' + redirectUrl;
        }
        res.writeHead(302, {//302 = temporary redirect
            'Location': redirectUrl
        });
        res.end();
    } else {
        var title = process.env.PAGE_TITLE || 'Server Down';
        var message = process.env.MESSAGE_HTML || '<h1>Server down</h1><p>It looks like something&rsquo;s up with the cloud. Our engineers are busily working to get it back up.</p><p>We apologize for the inconvenience and would appreciate it if you could come back later.</p>';
        res.writeHeader(200, {'Content-Type': 'text/html'});
        res.write('<html><head><title>' + title + '</title><style>body {font-family: Arial, Helvetica, sans-serif; padding: 20px;}</style></head><body>' + message + '</body></html>');
        res.end();
    }

}).listen(_port);
console.log('Server listening on port ' + _port);